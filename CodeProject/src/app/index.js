import React from 'react';
import {render} from 'react-dom';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';

import {Root} from "./components/Root"; 
import {Form} from "./components/Form";
import {List} from "./components/List";

class App extends React.Component{
	render(){
		return(

				<div>
					<Router history={browserHistory}>
						<Route path = "/" component = {Root}>
							<IndexRoute component={Form}/>
									<Route path = "list" component = {List}/>
									<Route path = "form" component = {Form}/>
							</Route>	
						<Route path = "form-single" component = {Form}/>
					</Router>
				</div>
			);
	}
}
render(<App/>, window.document.getElementById("app"));