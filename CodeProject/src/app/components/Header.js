import React from 'react';
import {Link} from 'react-router';

export const Header = (props) => {
		return(
				<nav className ="navbar navbar-default">
					<div className ="container">
						<div className ="navbar-header">
							<ul className ="nav navbar-nav ">
									<li><Link to = {"/form"}>Form</Link></li>
									<li><Link to = {"/list"}>List</Link></li>
							</ul>
						</div>
					</div>			
				</nav>	
	);
};